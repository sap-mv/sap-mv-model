package tests;

import static org.junit.Assert.assertEquals;
import org.junit.Test;
import dto.JefeVentas;

public class JefeVentasTest {
	
	JefeVentas jv1 = new JefeVentas(1,"Laura","Ferrero");

	@Test
	public void getIdJefeVentasTest() {
		assertEquals(1, jv1.getId());
	}
	
	@Test
	public void getNombreJefeVentasTest() {
		assertEquals("Laura", jv1.getNombre());
	}
	
	@Test
	public void getApellidoJefeVentasTest() {
		assertEquals("Ferrero", jv1.getApellido());
	}	
	
	@Test
	public void setIdJefeVentasTest() {
		jv1.setId(3);
		assertEquals(3, jv1.getId());
	}
	
	@Test
	public void setNombreJefeVentasTest() {
		jv1.setNombre("Maria");
		assertEquals("Maria", jv1.getNombre());
	}
	
	@Test
	public void setApellidoJefeVentasTest() {
		jv1.setApellido("Gomez");
		assertEquals("Gomez", jv1.getApellido());
	}
}