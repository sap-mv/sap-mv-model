package tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Test;
import grafo.Grafo;

public class GrafoTest {
	
	@Test(expected = IllegalArgumentException.class)
	public void verticeNegativoTest(){
		Grafo grafo = new Grafo(4);
		grafo.agregarArista(-1, 2);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void verticeExcedidoTest(){
		Grafo grafo = new Grafo(4);
		grafo.agregarArista(2, 4);
	}

	@Test(expected = IllegalArgumentException.class)
	public void sinBuclesTest(){
		Grafo grafo = new Grafo(4);
		grafo.agregarArista(2, 2);
	}
	
	@Test
	public void aristaExistenteTest(){
		Grafo grafo = new Grafo(4);
		grafo.agregarArista(0, 2);
		assertTrue(grafo.existeArista(0, 2));
	}

	@Test
	public void aristaInexistenteTest(){
		Grafo grafo = new Grafo(4);
		grafo.agregarArista(0, 2);
		assertFalse(grafo.existeArista(0, 3));
	}

	@Test(expected = IllegalArgumentException.class)
	public void gradoDeVerticeNegativoTest(){
		Grafo grafo = grafoPrueba();
		grafo.grado(-1);
	}
	
	@Test
	public void agregarAristaExistenteTest(){
		Grafo grafo = new Grafo(4);
		grafo.agregarArista(0, 3);
		grafo.agregarArista(0, 3);
		assertTrue(grafo.existeArista(0, 3));
	}

	@Test(expected = IllegalArgumentException.class)
	public void gradoDeVerticeExcedidoTest(){
		Grafo grafo = grafoPrueba();
		grafo.grado(5);
	}
	
	@Test
	public void gradoTest(){
		Grafo grafo = grafoPrueba();
		assertEquals(2,grafo.grado(0));
		assertEquals(1,grafo.grado(1));
		assertEquals(2,grafo.grado(2));
	}

	@Test 
	public void tamanoTest() {
		Grafo grafo = grafoPrueba();
		assertEquals(4, grafo.tamano());
	}
	
	private Grafo grafoPrueba() {
		Grafo grafo = new Grafo(4);
		grafo.agregarArista(0, 1);
		grafo.agregarArista(0, 2);
		grafo.agregarArista(2, 3);
		return grafo;
	}
}