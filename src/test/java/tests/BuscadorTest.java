package tests;

import static org.junit.Assert.assertEquals;
import java.io.File;
import java.io.IOException;
import java.util.jar.JarFile;
import org.junit.Test;
import discovery.Buscador;

public class BuscadorTest {

//	@Test(expected = IllegalArgumentException.class)
//	public void pathNoEsUnDirectorio() throws IOException {
//		Buscador sf = new Buscador();
//		File file = new File("resources/archivo.txt");
//		sf.buscarJars(file);
//	}
	
	@Test
	public void buscarJarsLibs() {
		Buscador sf = new Buscador();
		File folderLibs = new File("libs");
		assertEquals(1, sf.buscarJars(folderLibs).size());
	}
	
	
	@Test(expected = NullPointerException.class)
	public void buscarClasesEnJarNull() {
		Buscador sf = new Buscador();
		sf.buscarJars(null);
	}
	
	@Test
	public void buscarClasesEnJar() throws IOException {
		Buscador sf = new Buscador();
		JarFile folderJar = new JarFile("libs/slob.jar");
		assertEquals(1, sf.buscarClases(folderJar).size());
	}
	
	@Test
	public void sacarExtensionArchivoClass() {
		String archivoClass = "Serializer.class";
		assertEquals("Serializer", Buscador.sacarExtensionClass(archivoClass));
	}
}