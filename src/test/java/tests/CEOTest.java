package tests;

import static org.junit.Assert.assertEquals;
import org.junit.Test;
import dto.CEO;

public class CEOTest {

	CEO c1 = new CEO(1,"Juan","Perez");

	@Test
	public void getIdCEOTest() {
		assertEquals(1, c1.getId());
	}
	
	@Test
	public void getNombreCEOTest() {
		assertEquals("Juan", c1.getNombre());
	}
	
	@Test
	public void getApellidoCEOTest() {
		assertEquals("Perez", c1.getApellido());
	}	
	
	@Test
	public void setIdCEOTest() {
		c1.setId(2);
		assertEquals(2, c1.getId());
	}
	
	@Test
	public void setNombreCEOTest() {
		c1.setNombre("Luis");
		assertEquals("Luis", c1.getNombre());
	}
	
	@Test
	public void setApellidoCEOTest() {
		c1.setApellido("Hernandez");
		assertEquals("Hernandez", c1.getApellido());
	}
}