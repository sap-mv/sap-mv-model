package tests;

import static org.junit.Assert.assertEquals;
import org.junit.Test;
import dto.Persona;

public class PersonaTest {
	
	Persona p1 = new Persona(1,"Jorge","Gonzalez");
	
	@Test
	public void getIdPersonaTest() {
		assertEquals(1, p1.getId());
	}
	
	@Test
	public void getNombrePersonaTest() {
		assertEquals("Jorge", p1.getNombre());
	}
	
	@Test
	public void getApellidoPersonaTest() {
		assertEquals("Gonzalez", p1.getApellido());
	}

	@Test
	public void setIdPersonaTest() {
		p1.setId(2);
		assertEquals(2, p1.getId());
	}
	
	@Test
	public void setNombrePersonaTest() {
		p1.setNombre("Juan");
		assertEquals("Juan", p1.getNombre());
	}
	
	@Test
	public void setApellidoPersonaTest() {
		p1.setApellido("Gomez");
		assertEquals("Gomez", p1.getApellido());
	}
}