package iteracion0;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.jar.JarFile;
import java.util.logging.Level;

import org.junit.Test;

import discovery.Buscador;

//
//import static org.junit.Assert.assertEquals;
//import static org.junit.Assert.assertTrue;
//import java.io.File;
//import java.io.IOException;
//import java.util.List;
//import java.util.Set;
//import java.util.jar.JarFile;
//
//import org.junit.Test;
//import discovery.BuscadorClases;
//import discovery.ManejoArchivo;
//import discovery.SerializerFinder;
//
public class UserStory1 {

	@Test
	public void criterioAceptacion1Test() throws Exception {
		Buscador buscador = new Buscador();
		JarFile carpeta = new JarFile("projects/empresa.jar");
		List<Class> lista = buscador.buscarClases(carpeta);
		assertEquals(3,lista.size());
	}

	@Test
	public void criterioAceptacion2Test() throws IOException {
		Buscador buscador = new Buscador();
		JarFile carpeta = new JarFile("projects/empresa.jar");
		List<Class> lista = buscador.buscarClases(carpeta);
		Class persona = lista.get(0);
		Class ceo = lista.get(1);
		Class jefeVentas = lista.get(2);
		assertTrue(persona.isAssignableFrom(ceo));
		assertTrue(persona.isAssignableFrom(jefeVentas));
		assertFalse(jefeVentas.isAssignableFrom(ceo));
	}
	
	@Test 
	public void criterioAceptacion3Test() throws Exception {
		Buscador buscador = new Buscador();
		JarFile jarTxt = new JarFile("resources/archivo.jar");
		assertEquals(0, buscador.buscarClases(jarTxt).size());
	}
	
	@Test(expected = NullPointerException.class)
	public void criterioAceptacion4Test() throws IOException {
		Buscador buscador = new Buscador();
		buscador.buscarClases(null);
	}
	
	@Test(expected = NullPointerException.class)
	public void criterioAceptacion5Test() throws IOException {
		Buscador buscador = new Buscador();
		buscador.buscarJars(null);
	}
	@Test(expected = IllegalArgumentException.class)
	public void criterioAceptacion6Test() throws IOException {
		Buscador buscador = new Buscador();
		File file = new File("resources/archivo.txt");
		buscador.buscarJars(file);
	}
		
	@Test
	public void criterioAceptacion7Test() throws IOException {
		Buscador buscador = new Buscador();
		JarFile folderJar = new JarFile("libs/slob.jar");
		List<Class> proyectoSlob = buscador.buscarClases(folderJar);
		assertEquals("persistencia.Serializador", proyectoSlob.get(0).getName());
	}
	

}