package iteracion1;

import static org.junit.Assert.assertEquals;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import dto.CEO;
import dto.Persona;
import grafo.Grafo;
import grafo.GrafoDirigido;
import tests.Assert;

public class UserStory2 {

	//Grado de vecinos de Persona
	@Test
	public void criterioAceptacion1Test() throws ClassNotFoundException, InstantiationException, IllegalAccessException {
		List<Persona> empleados = pruebaEmpleados();
		
		GrafoDirigido gd = new GrafoDirigido(empleados);
		Grafo g = gd.crearGrafo(empleados);
		
		assertEquals(2, g.grado(0));
	}
	
	//Grado de vecinos de CEO
	@Test
	public void criterioAceptacion2Test() throws ClassNotFoundException, InstantiationException, IllegalAccessException {
		List<Persona> empleados = pruebaEmpleados();
		
		GrafoDirigido gd = new GrafoDirigido(empleados);
		Grafo g = gd.crearGrafo(empleados);
		
		assertEquals(1, g.grado(1));
		assertEquals(1, g.grado(2));
	}
	
	//Vecinos de Persona
	@Test
	public void criterioAceptacion3Test() throws ClassNotFoundException, InstantiationException, IllegalAccessException {
		List<Persona> empleados = pruebaEmpleados();
		
		GrafoDirigido gd = new GrafoDirigido(empleados);
		Grafo g = gd.crearGrafo(empleados);
		
		Assert.setsIguales(new int[]{1,2},g.vecinos(0));
	}
	
	//Vecinos de CEO
	@Test
	public void criterioAceptacion4Test() throws ClassNotFoundException, InstantiationException, IllegalAccessException {
		List<Persona> empleados = pruebaEmpleados();
		
		GrafoDirigido gd = new GrafoDirigido(empleados);
		Grafo g = gd.crearGrafo(empleados);
		
		Assert.setsIguales(new int[]{0}, g.vecinos(1));
		Assert.setsIguales(new int[]{0}, g.vecinos(2));
	}
	
	private List<Persona> pruebaEmpleados() {
		Persona p1 = new Persona(0,"Jorge","Gomez");
		CEO ceo1 = new CEO(1,"Laura","Gonzalez");
		CEO ceo2 = new CEO(2,"Maria","Ferrero");

		List<Persona> empleados = new ArrayList<Persona>();
		empleados.add(p1);
		empleados.add(ceo1);
		empleados.add(ceo2);
		return empleados;
	}
}