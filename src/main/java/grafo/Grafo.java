package grafo;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class Grafo {
	
	//Representacion del grafo por lista de vecinos
	private ArrayList<HashSet<Integer>> vecinos;
		
	public Grafo(int cantVertices){
		this.vecinos = new ArrayList<HashSet<Integer>>(cantVertices);
			
		for(int i = 0; i < cantVertices; i++){
			this.vecinos.add(new HashSet<Integer>());
		}
	}
		
	public void agregarArista(int i,int j){
		verificarVertice(i,"agregar una arista");
		verificarVertice(j,"agregar una arista");
		verificarDistintos(i,j,"agregar una arista");
		
		vecinos.get(i).add(j);
		vecinos.get(j).add(i);
	}
	
	public boolean existeArista(int i,int j){
		verificarVertice(i,"consultar una arista");
		verificarVertice(j,"consultar una arista");
		return vecinos.get(i).contains(j);
	}
	
	//Grado de un vertice (cantidad de vecinos)
	public int grado(int i){
		return vecinos(i).size();
	}
	
	// Conjunto de vecinos de un vertice
	public Set<Integer> vecinos(int i){
		verificarVertice(i,"consultar vecinos");
		
		Set<Integer> ret = new HashSet<Integer>();
		for(int j = 0; j < this.tamano(); ++j) {
			if( i != j ){
				if( this.existeArista(i,j) ) {
					ret.add(j);
				}	
			}
		}
		return ret;
	}

	//Cantidad de vertices
	public int tamano(){
		return vecinos.size();
	}
	
	//Lanza una excepcion si el indice v esta fuera de rango para los vertices
	private void verificarVertice(int v, String accion){
		if( v < 0 )
			throw new IllegalArgumentException("El vertice no puede ser negativo: " + v);
		
		if( v >= tamano())
			throw new IllegalArgumentException("Los vertices deben estar entre 0 y |V|-1: " + v);
	}
	
	//Lanza una excepcion si los indices son iguales
	private void verificarDistintos(int i,int j,String accion){
		if(i == j){
			throw new IllegalArgumentException("Se intento "+ accion +" con los dos vertices iguales. Vertice=" + i );
		}
	}
}