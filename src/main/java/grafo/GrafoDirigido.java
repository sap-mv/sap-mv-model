package grafo;

import java.util.List;

import dto.Persona;

public class GrafoDirigido {
	
	private List<Persona> empleados;
	
	public GrafoDirigido(List<Persona> empleados) {
		this.empleados = empleados;
	}
	
	public Grafo crearGrafo(List<Persona> empleados) throws ClassNotFoundException, InstantiationException, IllegalAccessException {
		Grafo grafo = new Grafo(empleados.size());
		for(int i = 0; i < empleados.size(); ++i) {
			for(int j = i; j < empleados.size(); ++j) {
				if(i!=j) {
					
//					System.out.println(empleados.get(j).getNombre());
					Class c = empleados.get(i).getClass();
//					System.out.println("clase c1" + c.getName() + " empleado: " + empleados.get(i).getNombre());
					Class c2 = empleados.get(j).getClass();
//					System.out.println("clase c2" + c2.getName() + " empleado: " + empleados.get(j).getNombre());
//					
					if(c.isAssignableFrom(c2) && !c.equals(c2)) {
						grafo.agregarArista(empleados.get(i).getId(), empleados.get(j).getId());
					}
				}

			}
		}
		return grafo;
	}

	public List<Persona> getEmpleados() {
		return empleados;
	}
}