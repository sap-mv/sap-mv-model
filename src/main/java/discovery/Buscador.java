package discovery;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;


public class Buscador {

	public Buscador() {
	
	}
	
	public List<JarFile> buscarJars(File path) {
		if(path == null) {
			throw new NullPointerException("No debe ser null");
		}
		
		if(!path.isDirectory()) {
			throw new IllegalArgumentException("No es un directorio");
		}		
		List<JarFile> jars = new ArrayList<JarFile>();
		for (File f : path.listFiles()) {
			JarFile jarFile = null;
			try {
				if(f.getName().endsWith(".jar")) {
					jarFile = new JarFile(f);
					jars.add(jarFile);
				}
			} catch (IOException e) {
				//Log.escribirLog("El archivo " + f.getName() + " no es un .jar");
			}	
		}				
		return jars;
	}
	
	
	public List<Class> buscarClases(JarFile jar){
		if(jar == null) {
			throw new NullPointerException("El jar no debe ser null");		
		}
		
		ArrayList<Class> classes = new ArrayList<Class>();
		try {
			Enumeration<JarEntry> entries = jar.entries();
			while (entries.hasMoreElements()) {
				JarEntry entry = entries.nextElement();
				 if(entry.getName().endsWith(".class")) {
					 String classFile = sacarExtensionClass(entry.getName());
					 Class c = Class.forName(classFile);
					 classes.add(c);     	  	 	  
				 }
			}
	
		} catch (ClassNotFoundException e) {
					e.printStackTrace();
		}
		return classes;		
	}
	
	public static String sacarExtensionClass(String file) {
		String archivoClass =".class";
		String fileName = file.replaceAll("\\/", "\\.");
		return fileName.substring(0, file.length() - archivoClass.length());
	}

}
