package dto;

public class CEO extends Persona{
	
	private static final long serialVersionUID = 1L;
	
	public CEO(int id, String nombre, String apellido){
		super(id,nombre,apellido);
	}

}